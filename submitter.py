import argparse
from pathlib import Path
import subprocess


def make_args():
    description = 'Example job script'
    parser = argparse.ArgumentParser(description=description, )

    parser.add_argument('inputdir',
                        help='path to input directory/file',
                        type=Path)

    parser.add_argument('outputdir',
                        help='path to output directory (where daily ndjson .gz are saved)',
                        type=Path)

    parser.add_argument('-j',
                        '--jobname',
                        default='',
                        type=str)

    return parser.parse_args()


def main():
    args = make_args()

    print(f'Running job name: {args.jobname}')

    param_sets = []
    with open(args.inputdir, 'r') as f:
        for line in f.readlines():
            param_sets.append(line.rstrip('\n'))

    for params in param_sets:
        p1, p2 = params.split(' ')
        submit_string = '/usr/bin/sbatch '
        submit_string += f'--job-name={args.jobname}{params.replace(" ", "_")} '
        submit_string += f'--output={args.outputdir}/{p1}_{p2}.log '
        submit_string += f'--export=ALL,PARAM1={p1},PARAM2={p2},OUTPUTDIR={args.outputdir},INPUTDIR={args.inputdir} '
        submit_string += 'subscript.sbatch '

        subprocess.call([submit_string], shell=True)


if __name__ == "__main__":
    main()
