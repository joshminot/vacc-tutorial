# VACC Tutorial

# Contents

name | desc.
--- | ---
`job.py` | example job script
`submitter.py` | python submitter that takes 2 column .txt file and passes params to `job.py` (via `subscript.sbatch`)
`example_params.txt` | example parameters file for `job.py`
`subscript.sbatch` | sbatch script for running `job.py`

## Basic SLURM commands

* List job you have on queue: `squeue --me`
* Submit job: `sbatch <slurm_script_name>`
* Cancel job: `scancel <job_id>` (get job ID from `squeue`)

## Example

```python submitter.py ./example_params.txt ./output```


## Resources

* [SLURM cheatsheet](https://slurm.schedmd.com/pdfs/summary.pdf)

