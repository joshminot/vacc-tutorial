import sys
import argparse
from pathlib import Path


def make_args():
    description = 'Example job script'
    parser = argparse.ArgumentParser(description=description, )

    parser.add_argument('inputdir',
                         help='path to output directory',
                         type=Path)

    parser.add_argument('outputdir',
                         help='path to output directory (where daily ndjson .gz are saved)',
                         type=Path)

    parser.add_argument('-k',
                         '--param_k',
                         default=None,
                         type=str)

    parser.add_argument('-r',
                         '--param_r',
                         default=None,
                         type=str)
    return parser.parse_args()


def main():
    args = make_args()

    print(f'Running job number: {args.param_r}')

    save_name = args.param_r + args.param_k if args.param_r and args.param_k else 'NoName'

    with open(args.outputdir / f'output_{save_name}.txt', 'w') as f:
        f.write(save_name)


if __name__ == "__main__":
    main()
